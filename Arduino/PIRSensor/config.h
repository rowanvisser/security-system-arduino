#ifndef _CONFIG_H
#define _CONFIG_H

// Change project name
#define PROJECT_SHORT_NAME "Security System PIR Sensor"

// Change default SSID
#define CONFIG_SSID "SS_PIR_SENSOR"

#define PIR D7 

//API
#define SERVER_URL "http://192.168.1.230:3000"
#define GET_POWER_STATUS "/v1/power/sensor/"
#define POST_LOG "/v1/logs/"

#define DRD_TIMEOUT 0.5
#define DRD_ADDRESS 0

#endif
