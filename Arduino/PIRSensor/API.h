String SENSORID;

String postLog(String type, String description){
   HTTPClient http; //Declare object of class HTTPClient
   String postData = String("" + type + "/" + description);
   Serial.println(SERVER_URL + String(POST_LOG) + postData);
   http.begin(SERVER_URL + String(POST_LOG) + postData); //Specify request destination
   int httpCode = http.POST("Type/Description"); //Send the request
   String payload = http.getString();  //Get the response payload
 
   Serial.println(httpCode); //Print HTTP return code
   Serial.println(payload); //Print request response payload 
}

String getPowerStatus(){
  HTTPClient http;
  http.begin(SERVER_URL + String(GET_POWER_STATUS) +String(SENSORID));
  uint16_t httpCode = http.GET();
  String apiStatus;
  if (httpCode == 200)
  {
    Serial.println("GET POWER STATUS SUCCESFUL");
    apiStatus = http.getString();
  }else{
    Serial.println("ERROR GET POWER STATUS");
    Serial.println(httpCode);
  }
  Serial.println("System status in database = " + apiStatus);
  return apiStatus;
}
