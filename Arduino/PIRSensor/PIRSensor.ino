#include <DoubleResetDetector.h> 
#include <ArduinoJson.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include <WiFiManager.h>
#include "config.h"
#include "API.h"
#include "WifiConfig.h"

//Calls to constructors so we can use those classes
DoubleResetDetector drd(DRD_TIMEOUT, DRD_ADDRESS); 
WiFiManager wifiManager;
bool systemOn = false;
int pirValue;

void setup()
{
  Serial.begin(115200);
  pinMode(PIR, INPUT);
  SENSORID = generateChipID();
  String configSSID = String(CONFIG_SSID) + "_" + SENSORID;
  wifiManager.autoConnect(configSSID.c_str());
  
  //If you doubleclick on the reset button it will reset the wifisettings 
  if (drd.detectDoubleReset()) {
    Serial.println("Double Reset Detected");
    resetWifiSettings();
  }
  
  postLog(SENSORID, "StartupSensor");
  delay(500);

  //Stop checking to the double click reset wifisettings
  drd.stop();
  
  Serial.println("Sensor succesfully started");
}


// main loop
void loop()
{
  delay(200);
  String status = getPowerStatus();
  if(systemOn){
    if(status != "1"){
      systemOn = false;
    }
  }else{
    if(status == "1"){
      systemOn = true;
    }
  }
  delay(200);
  pirValue = digitalRead(PIR);
  if(pirValue == HIGH){
    if(systemOn){
      detected();
      Serial.println("PIR DETECTED AND SYSTEM ON");
    }
  }
  delay(2000);
}


void detected(){
  Serial.println("Detected");
  String status = getPowerStatus();
  delay(1000);
  if(status == "1"){
    delay(1000);
    postLog(SENSORID, "Detected");
    delay(5000); 
  }
}


void resetWifiSettings(){
      wifiManager.resetSettings();
      ESP.reset();
}
