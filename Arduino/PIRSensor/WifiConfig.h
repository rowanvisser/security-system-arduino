String generateChipID()
{
  String chipIDString = String(ESP.getChipId() & 0xffff, HEX);
  
  chipIDString.toUpperCase();
  Serial.print("CHIPID ");
  Serial.println(chipIDString);
  while (chipIDString.length() < 4)
    chipIDString = String("0") + chipIDString;

  return chipIDString;
}
