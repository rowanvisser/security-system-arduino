//Libraries
#include <DoubleResetDetector.h> 
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>        
#include <WiFiManager.h> 
#include <ESP8266WiFi.h> 
//Extra Classes
#include "pitches.h"
#include "config.h"
#include "WifiConfig.h"
#include "API.h"

//Calls to constructors so we can use those classes
DoubleResetDetector drd(DRD_TIMEOUT, DRD_ADDRESS);        
WiFiManager wifiManager;

//Variable declarations
String chipId = generateChipID();                         //Chip ID is used to make WIFI connection to the right Endian
String configSSID = String(CONFIG_SSID) + "_" + chipId;   //Config SSID is the name of the endian with the Chipid we have created
int melody[] = {NOTE_C5, NOTE_D5, NOTE_E5, NOTE_F5, NOTE_G5, NOTE_A5, NOTE_B5, NOTE_C6};
bool systemDetectedOn = false;

//This method is the first method that runs in this main class automaticly
void setup()
{  
  //Setup serial monitor on 115200 BAUD
  Serial.begin(115200);
  SENSORID = generateChipID();
  //If he can find a wifi network that is already configured it will go automaticly
  //Otherwise you have to set it up in your wifi settings, it will make an accesspoint
  wifiManager.autoConnect(configSSID.c_str());
  
  //If you doubleclick on the reset button it will reset the wifisettings 
  if (drd.detectDoubleReset()) {
    Serial.println("Double Reset Detected");
    resetWifiSettings();
  }
  
  //Stop checking to the double click reset wifisettings
  drd.stop();
}

//The loop is the second method that will automaticly runs, this will run as a loop
void loop()
{

  delay(1000);
  String status = getDetection();
  delay(1000);
  if(status == "1"){
    systemDetectedOn = true;
    detected();
  }
  delay(10000);
}

void detected(){
  Serial.println("Detected");
  while(systemDetectedOn){
    tone(BUZZER, NOTE_G5, 1500);
    delay(1600); 
    String status = getPowerStatus();
    delay(1000);
    if(status == "0"){
      systemDetectedOn = false;
    }
  }
}


void resetWifiSettings(){
      wifiManager.resetSettings();
      ESP.reset();
}
