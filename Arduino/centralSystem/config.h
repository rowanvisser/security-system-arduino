#ifndef _CONFIG_H
#define _CONFIG_H

// Change project name
#define PROJECT_SHORT_NAME "Security System"

// Change server name
#define SERVER_URL "http://192.168.1.230:3000"

// Change default SSID
#define CONFIG_SSID "SS_CENTRAL"

//PINS
#define BUZZER D8


//for double reset button input
#define DRD_TIMEOUT 0.5
#define DRD_ADDRESS 0

//API CALLS
#define GET_POWER_STATUS "/v1/central/power/"
#define GET_DETECTION "/v1/central/"

#endif
