String SENSORID;

String getDetection(){
  HTTPClient http;
  http.begin(SERVER_URL + String(GET_DETECTION) +String(SENSORID));
  uint16_t httpCode = http.GET();
  String apiStatus;
  if (httpCode == 200)
  {
    Serial.println("GET DETECTION STATUS SUCCESFUL");
    apiStatus = http.getString();
  }else{
    Serial.println("ERROR GET DETECTION STATUS");
    Serial.println(httpCode);
  }
  Serial.println("Detection status in database = " + apiStatus);
  return apiStatus;
}

String getPowerStatus(){
  HTTPClient http;
  http.begin(SERVER_URL + String(GET_POWER_STATUS) +String(SENSORID));
  uint16_t httpCode = http.GET();
  String apiStatus;
  if (httpCode == 200)
  {
    Serial.println("GET POWER STATUS SUCCESFUL");
    apiStatus = http.getString();
  }else{
    Serial.println("ERROR GET POWER STATUS");
    Serial.println(httpCode);
  }
  Serial.println("System status in database = " + apiStatus);
  return apiStatus;
}
