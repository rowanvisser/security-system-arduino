//This class is for controlling the LED indicator on the PCB
//We make only use of switchLedStatus to control the led, with defined led statusses in the settings.h
#include <Adafruit_NeoPixel.h> // Library we use to control the LED

class LED {
  public:
    LED(){
      
    }

  void ledIndicator(String status){
    if(status == "standby"){
      analogWrite(RGB_RED, 255);
      analogWrite(RGB_GREEN, 255);
      analogWrite(RGB_BLUE, 0);
      Serial.println("Led Standby");      
    }else if(status == "on"){
      analogWrite(RGB_RED, 0);
      analogWrite(RGB_GREEN, 255);
      analogWrite(RGB_BLUE, 0); 
      Serial.println("Led On");     
    }else if(status == "detected"){
      analogWrite(RGB_RED, 255);
      analogWrite(RGB_GREEN, 0);
      analogWrite(RGB_BLUE, 0);
      Serial.println("Led Detected");
    }else if(status == "starting"){
      analogWrite(RGB_RED, 255);
      analogWrite(RGB_GREEN, 255);
      analogWrite(RGB_BLUE, 255);
      Serial.println("Led Starting");
    }else if(status == "off"){
      analogWrite(RGB_RED, 0);
      analogWrite(RGB_GREEN, 0);
      analogWrite(RGB_BLUE, 0);
      Serial.println("Led Off");
    }
  }
};
