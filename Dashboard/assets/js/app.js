const API_ADDRESS = "http://localhost:3000/v1/"

//Getting all the logs
function getLogs(){
  $.ajax({
    url: API_ADDRESS +"logs/detection/0",
    async: false,
    type: "GET",
    dataType: "json",
    success: function (result) {
      console.log(result);
        // if(result.length != 0 && result.length != sensor_log_length){
          var event_data = '';
          // sensor_log_length = result.length;
          $(".log-item").remove();
          $.each(result, function(index, value){;
            /*console.log(value);*/
            event_data += '<tr class="log-item">';
            event_data += '<td>'+value.zone_name+'</td>';
            event_data += '<td>'+value.sensor_id+'</td>';
            event_data += '<td>'+value.type+'</td>';
            event_data += '<td>'+value.description+'</td>';
            event_data += '<td>'+value.date+'</td>';
            event_data += '</tr>';
          });
          $("#log_table").append(event_data);
        // }
      },
      error: function (xhr, ajaxOptions, thrownError) {
        console.log(xhr);
      },
      complete: function() {
        // Schedule the next request when the current one's complete
        setTimeout(getLogs, 15000);
      }
    });
};

//Get the amount of all the logs
function getLogsAmount(){
  $.ajax({
    url: API_ADDRESS +"logs/all/0",
    async: false,
    type: "GET",
    dataType: "json",
    success: function (result) {
      console.log(result);
      $("#logs").html("<b>"+result.length+"</b>");
    },
    error: function (xhr, ajaxOptions, thrownError) {
      console.log(xhr);
    },
    complete: function() {
        // Schedule the next request when the current one's complete
        setTimeout(getLogsAmount, 15000);
      }
    });
};

//get 10 most newest logs
function getDetectionLogs(){
  $.ajax({
    url: API_ADDRESS +"logs/detection/10",
    async: false,
    type: "GET",
    dataType: "json",
    success: function (result) {
      console.log(result);
      var event_data = '';
      $(".log-item").remove();
      $.each(result, function(index, value){;
        /*console.log(value);*/
        event_data += '<tr class="log-item">';
        event_data += '<td>'+value.zone_name+'</td>';
        event_data += '<td>'+value.sensor_id+'</td>';
        event_data += '<td>'+value.type+'</td>';
        event_data += '<td>'+value.description+'</td>';
        event_data += '<td>'+value.date+'</td>';
        event_data += '</tr>';
      });
      $("#log_table").append(event_data);
        // }
      },
      error: function (xhr, ajaxOptions, thrownError) {
        console.log(xhr);
      },
      complete: function() {
        // Schedule the next request when the current one's complete
        setTimeout(getDetectionLogs, 15000);
      }
    });
};

//Checks if we can connect to the API
function getApiStatus(){
  $.ajax({
    url: API_ADDRESS +"zone/",
    async: false,
    type: "GET",
    dataType: "json",
    success: function (result) {
      $("#welcome").html("<b>System is up and running</b>");
    },
    error: function (xhr, ajaxOptions, thrownError) {
      console.log(xhr);
      $("#welcome").html("<b>No connection to system</b>");
    }
  });    
};

//Get external weather
function getWeather(status, zone){
  $.ajax({
    url: API_ADDRESS +"weather",
    async: false,
    type: "GET",
    success: function (result) {
      console.log("Get weather");
      console.log(result);
      $("#weather").html("<b>" + result + "&#8451; in Krommenie</b>");

    },
    error: function (xhr, ajaxOptions, thrownError) {
      $("#weather").html("<b>Could not load weather</b>");
      console.log(xhr);
    },
    complete: function() {
        // Schedule the next request when the current one's complete
        setTimeout(getWeather, 15000);
      }
    });
}; 

//For getting all the zones
function getZones(){
  $.ajax({
    url: API_ADDRESS +"zone/",
    async: false,
    type: "GET",
    dataType: "json",
    success: function (result) {
      var event_data = '';
      $(".zone-item").remove();
      $.each(result, function(index, value){;
        event_data += '<tr class="zone-item">';
        event_data += '<td>'+value.zone_id+'</td>';
        event_data += '<td>'+value.zone_name+'</td>';
        event_data += '<td>'+value.status+'</td>';
        if(value.status == 0){
          event_data += '<td><a href="#" onclick="zonePower(1,'+value.zone_id+');"><b style="color:green">Turn zone on</b></a> | <a href="#" onclick="deleteZone('+value.zone_id+');">Delete </a></td>';
        }else{
          event_data += '<td><a href="#" onclick="zonePower(0,'+value.zone_id+');"><b style="color:red">Turn zone off </b></a> | <a href="#" onclick="deleteZone('+value.zone_id+');">Delete</a></td>';
        }
        event_data += '</tr>';
      });
      $("#zone_table").append(event_data);
    },
    error: function (xhr, ajaxOptions, thrownError) {
      console.log(xhr);
    },
    complete: function() {
      setTimeout(getZones, 15000);
    }
  });
};

//Adding zones
function addZone(){
  const zone_name = document.getElementById("zone_name").value;
  $.ajax({
    url: API_ADDRESS +"zone/" + zone_name + "/add",
    type: "POST",
    success: function (result) {
      $("#responseMessage").html("<b style='color:green'>Added Sensor</b>");
    },
    error: function (xhr, ajaxOptions, thrownError) {
      $("#responseMessage").html("<b style='color:red'>Failed to add Sensor</b>");
    }
  });
};

//Change zone power status
function zonePower(status, zone){
  $.ajax({
    url: API_ADDRESS +"power/zone/" + zone + "/" + status,
    async: false,
    type: "POST",
    success: function (result) {
      console.log("ZONE POWER CHANGED");
      getZones();
    },
    error: function (xhr, ajaxOptions, thrownError) {
      console.log(xhr);
    }
  });
};

//For deleting a zone
function deleteZone(zone_id){
  $.ajax({
    url: API_ADDRESS +"zone/" + zone_id + "/delete",
    type: "POST",
    success: function (result) {
      $("#responseMessageDelete").html("<b style='color:green'>Succesfully deleted</b>");
      getZones();
    },
    error: function (xhr, ajaxOptions, thrownError) {
      $("#responseMessageDelete").html("<b style='color:red'>Failed deleting item</b>");
    }
  });
};

//For adding a sensor
function addSensor(){
  const sensor_id = document.getElementById("sensor_id").value;
  const type = document.getElementById("type").value;
  $.ajax({
    url: API_ADDRESS +"sensor/" + sensor_id + "/" + type,
    type: "POST",
    success: function (result) {
      $("#responseMessage").html("<b style='color:green'>Added Sensor</b>");
      getSensors();
    },
    error: function (xhr, ajaxOptions, thrownError) {
      $("#responseMessage").html("<b style='color:red'>Failed to add Sensor</b>");
    }
  });
};

//For getting the sensors
function getSensors(){
  $.ajax({
    url: API_ADDRESS +"sensors/",
    async: false,
    type: "GET",
    dataType: "json",
    success: function (result) {
      var event_data = '';
      $(".sensor-item").remove();
      $.each(result, function(index, value){;
        event_data += '<tr class="sensor-item">';
        event_data += '<td>'+value.sensor_id+'</td>';
        event_data += '<td>'+value.type+'</td>';
        event_data += '<td><a href="#" onclick="deleteSensor(\''+String(value.sensor_id)+'\')">Delete</a></td>';
        event_data += '</tr>';
      });
      $("#sensor_table").append(event_data);
    },
    error: function (xhr, ajaxOptions, thrownError) {
      console.log(xhr);
      $("#welcome").html("<b>No connection to system</b>");
    },
    complete: function() {
      setTimeout(getSensors, 15000);
    }
  });
};

//Deleting a sensor
function deleteSensor(sensor_id){
  $.ajax({
    url: API_ADDRESS +"sensor/" + sensor_id,
    type: "POST",
    success: function (result) {
      $("#responseMessageDelete").html("<b style='color:green'>Succesfully deleted</b>");
      getSensors();
    },
    error: function (xhr, ajaxOptions, thrownError) {
      $("#responseMessageDelete").html("<b style='color:red'>Failed deleting item</b>");
    }
  });
};


//For getting the zones with sensors
function getZoneSensors(){
  $.ajax({
    url: API_ADDRESS +"zone-sensor/",
    async: false,
    type: "GET",
    dataType: "json",
    success: function (result) {
      var event_data = '';
      $(".zone-sensor-item").remove();
      $.each(result, function(index, value){;
        event_data += '<tr class="zone-sensor-item">';
        event_data += '<td>'+value.zone_name+'</td>';
        event_data += '<td>'+value.sensor_id+'</td>';
        event_data += '<td><a href="#" onclick="deleteZoneSensor('+value.id+')">Delete</a></td>';

        event_data += '</tr>';
      });
      $("#zone-sensor_table").append(event_data);
    },
    error: function (xhr, ajaxOptions, thrownError) {
      console.log(xhr);
    },
    complete: function() {
      setTimeout(getZoneSensors, 15000);
    }
  });
};

//For adding a sensor to a zone
function addZoneSensor(){
  const sensor_id = document.getElementById("sensor_id").value;
  const zone_name = document.getElementById("zone_name").value;
  $.ajax({
    url: API_ADDRESS +"zone-sensor/" + zone_name + "/" + sensor_id,
    type: "POST",
    success: function (result) {
      $("#responseMessage").html("<b style='color:green'>Added Sensor</b>");
    },
    error: function (xhr, ajaxOptions, thrownError) {
      $("#responseMessage").html("<b style='color:red'>Failed to add Sensor</b>");
    }
  });
};

//For delete a sensor from a zone
function deleteZoneSensor(zoneSensor_id){
  $.ajax({
    url: API_ADDRESS +"zone-sensor/" + zoneSensor_id,
    type: "POST",
    success: function (result) {
      $("#responseMessageDelete").html("<b style='color:green'>Succesfully deleted</b>");
      getZoneSensors();
    },
    error: function (xhr, ajaxOptions, thrownError) {
      $("#responseMessageDelete").html("<b style='color:red'>Failed deleting item</b>");
    }
  });
};

//For getting all the zones and return there name to use in a select function
function getZonesList(){
  $.ajax({
    url: API_ADDRESS +"zone/",
    async: false,
    type: "GET",
    dataType: "json",
    success: function (result) {
      var event_data = '';
      for (var i = 0; i < result.length; i++){
        event_data += '<option value ='+ result[i].zone_id +'>'+ result[i].zone_name +'</td>';
      }
      $("#zone_name").append(event_data);
    },
    error: function (xhr, ajaxOptions, thrownError) {
      console.log(xhr);
    },
    complete: function() {
      setTimeout(getZones, 15000);
    }
  });
};

//For getting all the sensors and return there name to use in a select function
function getSensorsList(){
  $.ajax({
    url: API_ADDRESS +"sensors/",
    async: false,
    type: "GET",
    dataType: "json",
    success: function (result) {
      var event_data = '';
      for (var i = 0; i < result.length; i++){
        event_data += '<option value ='+ result[i].sensor_id +'>'+ result[i].sensor_id +'</td>';
      }
      $("#sensor_id").append(event_data);
    },
    error: function (xhr, ajaxOptions, thrownError) {
      console.log(xhr);
    },
    complete: function() {
      setTimeout(getSensors, 15000);
    }
  });
};

//For getting all the centrals
function getCentrals(){
  $.ajax({
    url: API_ADDRESS +"central/",
    async: false,
    type: "GET",
    dataType: "json",
    success: function (result) {
      var event_data = '';
      $(".central-item").remove();
      $.each(result, function(index, value){;
        event_data += '<tr class="central-item">';
        event_data += '<td>'+value.id+'</td>';
        event_data += '<td>'+value.central_id+'</td>';
        if(value.status == 0){
          event_data += '<td><a href="#" onclick="deleteCentral('+value.id+');">Delete </a></td>';
        }else{
          event_data += '<td><a href="#" onclick="deleteCentral('+value.id+');">Delete</a></td>';
        }
        event_data += '</tr>';
      });
      $("#central_table").append(event_data);
    },
    error: function (xhr, ajaxOptions, thrownError) {
      console.log(xhr);
    },
    complete: function() {
      setTimeout(getCentrals, 15000);
    }
  });
};

//For deleting a central
function deleteCentral(central_id){
  $.ajax({
    url: API_ADDRESS +"central/" + central_id + "/delete",
    type: "POST",
    success: function (result) {
      $("#responseMessageDelete").html("<b style='color:green'>Succesfully deleted</b>");
      getCentrals();
    },
    error: function (xhr, ajaxOptions, thrownError) {
      $("#responseMessageDelete").html("<b style='color:red'>Failed deleting item</b>");
    }
  });
};

//For adding a sensor
function addCentral(){
  const central_name = document.getElementById("central_name").value;
  $.ajax({
    url: API_ADDRESS +"central/" + central_name,
    type: "POST",
    success: function (result) {
      $("#responseMessage").html("<b style='color:green'>Added central</b>");
      getCentrals();
    },
    error: function (xhr, ajaxOptions, thrownError) {
      $("#responseMessage").html("<b style='color:red'>Failed to add central</b>");
    }
  });
};










//For getting the central with sensors
function getCentralZone(){
  $.ajax({
    url: API_ADDRESS +"central-zone/",
    async: false,
    type: "GET",
    dataType: "json",
    success: function (result) {
      console.log(result);
      var event_data = '';
      $(".central-zone-item").remove();
      $.each(result, function(index, value){;
        event_data += '<tr class="central-zone-item">';
        event_data += '<td>'+value.central_id+'</td>';
        event_data += '<td>'+value.zone_name+'</td>';
        event_data += '<td><a href="#" onclick="deleteCentralZone('+value.central_id+',' + value.zone_id + ')">Delete</a></td>';

        event_data += '</tr>';
      });
      $("#central-zone-table").append(event_data);
    },
    error: function (xhr, ajaxOptions, thrownError) {
      console.log(xhr);
    },
    complete: function() {
      setTimeout(getCentralZone, 15000);
    }
  });
};

//For adding a sensor to a zone
function addCentralZone(){
  const central_id = document.getElementById("central_id").value;
  const zone_name = document.getElementById("zone_name").value;
  $.ajax({
    url: API_ADDRESS +"central/" + central_id + "/" + zone_name,
    type: "POST",
    success: function (result) {
      $("#responseMessage").html("<b style='color:green'>Added Sensor</b>");
    },
    error: function (xhr, ajaxOptions, thrownError) {
      $("#responseMessage").html("<b style='color:red'>Failed to add Sensor</b>");
    }
  });
};

//For delete a sensor from a zone
function deleteCentralZone(central_id, zone_id){
  $.ajax({
    url: API_ADDRESS +"central-zone/" + central_id + "/" + zone_id,
    type: "POST",
    success: function (result) {
      $("#responseMessageDelete").html("<b style='color:green'>Succesfully deleted</b>");
      getCentralZone();
    },
    error: function (xhr, ajaxOptions, thrownError) {
      $("#responseMessageDelete").html("<b style='color:red'>Failed deleting item</b>");
    }
  });
};

//For getting all the sensors and return there name to use in a select function
function getCentralList(){
  $.ajax({
    url: API_ADDRESS +"central/",
    async: false,
    type: "GET",
    dataType: "json",
    success: function (result) {
      var event_data = '';
      for (var i = 0; i < result.length; i++){
        event_data += '<option value ='+ result[i].central_id +'>'+ result[i].central_id +'</td>';
      }
      $("#central_id").append(event_data);
    },
    error: function (xhr, ajaxOptions, thrownError) {
      console.log(xhr);
    },
    complete: function() {
      setTimeout(getSensors, 15000);
    }
  });
};


