const API_ADDRESS = "http://localhost:3000/v1/"

//For checking the credentials
function loginCheck(){
  const username = document.getElementById("username").value;
  const password = document.getElementById("password").value;
  $.ajax({
  url: API_ADDRESS + "user/" + username + "/" + password,
  async: false,
  type: "POST",
    success: function (result) {
      console.log("User check", result);
      $.ajax({ url: 'assets/login/login-check.php',
         data: {loginCheck: true},
         type: 'post',
         success: function(output) {
            $("#responseMessage").html("<b style='color:green'>Succesfully logged in</b>");
            location.reload();
         }
      });
    },
    error: function (xhr, ajaxOptions, thrownError) {
       $("#responseMessage").html("<b style='color:red'>Credentials are wrong</b>");
    }
  });
};