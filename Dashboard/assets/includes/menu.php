<nav class="navbar navbar-expand-md bg-dark navbar-dark">
  <a class="navbar-brand" href="#"><h2>Security System</h2></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse " id="collapsibleNavbar">
    <ul class="navbar-nav d-block d-md-none">
      <li class="nav-item">
        <a class="nav-link" href="index.php">Dashboard</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="logs.php">Logs</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
          Central
        </a>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="Central.php">All Centrals</a>
          <a class="dropdown-item" href="central-zone.php">Central Zone</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
          Zones
        </a>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="zones.php">All zones</a>
          <a class="dropdown-item" href="zone-sensor.php">Zone sensors</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="sensors.php">Sensors</a>
      </li> 
      <li class="nav-item">
        <a class="nav-link" href="assets/login/logout.php">Logout</a>
      </li>      
    </ul>
  </div>  
</nav>
<div class="container">
  <div class="row main">
    <div class="col-md-3 d-none d-md-block menu-side">
      <nav class="navbar">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Dashboard</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="logs.php">Logs</a>
          </li>
         <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
              Central
            </a>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="Central.php">All Centrals</a>
              <a class="dropdown-item" href="central-zone.php">Central Zone</a>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
              Zones
            </a>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="zones.php">All zones</a>
              <a class="dropdown-item" href="zone-sensor.php">Zone sensors</a>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="sensors.php">Sensors</a>
          </li> 
                <li class="nav-item">
            <a class="nav-link" href="assets/login/logout.php">Logout</a>
          </li> 
        </ul>
      </nav>
    </div>
    <div class="col-md-9 col-sm-12 content-side">