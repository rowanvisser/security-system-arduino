<?php 
require_once 'assets/includes/head.php';
?>

<body onload="onLoad()">

  <?php 
  require_once 'assets/includes/menu.php';
  ?>

  <div class="row content">
    <div class="col-sm-4 info-container">
      <h2>Welcome</h2>
      <h6><b id="welcome">Loading</b></h6>

    </div>
    <div class="col-sm-4 info-container">
      <h2>Total logs</h2>
      <h4><b id="logs">Loading</b></h4>
    </div>
    <div class="col-sm-4 info-container">
      <h2>Weather</h2>
      <h6><b id="weather">Loading</b></h6>
    </div>
  </div>
  <hr>
  <div class="row content">
    <div class="col-sm-12">
      <h2>Newest detection logs</h2>
      <div class="table-responsive">          
        <table class="table table-dark table-striped" id="log_table">
          <thead>
            <tr>
              <th>Zone</th>
              <th>Sensor_ID</th>
              <th>Type</th>
              <th>Description</th>
              <th>Date</th>
              <!-- <th>Country</th> -->
            </tr>
          </thead>
          <tbody>
            <tr>

            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <?php 
  require_once 'assets/includes/footer.php';
  ?>
  <script src="assets/js/app.js"></script>
  <script>
    function onLoad(){
      getApiStatus();
      getDetectionLogs();
      getLogsAmount();
      getWeather();
    }; 
  </script>
</html>
