<?php
session_start();
if(isset($_SESSION['login'])){
header("location: index.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Security System Login</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <!-- theme stylesheet-->
  <link rel="stylesheet" href="./assets/style/login.css" id="login-stylesheet">

</head>
<head>

</head>
<body class="login">
  <div class="container">
    <div class="row">
      <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
        <div class="card card-signin my-5">
          <div class="card-body">
            <h2 class="text-center">Security System</h2>
            <hr>
            <h5 class="card-title text-center">Sign In</h5>
            <h6><b id="responseMessage"></b></h6>
            <form class="form-signin">
              <div class="form-label-group">
                <input type="text" id="username" class="form-control" placeholder="Username" required autofocus>
                <label for="username">Username</label>
              </div>

              <div class="form-label-group">
                <input type="password" id="password" class="form-control" placeholder="Password" required>
                <label for="password">Password</label>
              </div>

              <button onclick="loginCheck()" class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Sign in</button>
              <hr>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="assets/js/login.js"></script>
</html>