<?php 
require_once 'assets/includes/head.php';
?>

<body onload="onLoad()">

  <?php 
  require_once 'assets/includes/menu.php';
  ?>

  <div class="row content">
    <div class="col-sm-12">
      <h2>All Logs</h2>
      <hr>
      <input class="form-control" id="searchInput" type="text" placeholder="Search..">
      <br>
      <div class="table-responsive">          
        <table class="table table-dark table-striped" id="log_table">
          <thead>
            <tr>
              <th>Zone</th>
              <th>Sensor_ID</th>
              <th>Type</th>
              <th>Description</th>
              <th>Date</th>
              <!-- <th>Country</th> -->
            </tr>
          </thead>
          <tbody>
            <tr>

            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <?php 
  require_once 'assets/includes/footer.php';
  ?>
  <script src="assets/js/app.js"></script>
  <script>
    $(document).ready(function(){
      $("#searchInput").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#log_table tr").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });
    });
    function onLoad(){
      getLogs();
    };
  </script>
</body>
</html>
