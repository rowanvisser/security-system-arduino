<?php 
require_once 'assets/includes/head.php';
?>

<body onload="onLoad()">

  <?php 
  require_once 'assets/includes/menu.php';
  ?>
  <div class="row content">
    <div class="col-sm-12 content-block">
      <h2>Zone Sensors</h2>
      <h6><b id="responseMessageDelete"></b></h6>
      <div class="table-responsive">          
        <table class="table table-dark table-striped" id="zone-sensor_table">
          <thead>
            <tr>
              <th>Zone Name</th>
              <th>Sensor ID</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <tr>

            </tr>
          </tbody>
        </table>
      </div>
      <hr>
      <h2>Add sensor to zone</h2>
      <h6><b id="responseMessage"></b></h6>
      <form class="form-inline">
        <label for="zone_name" class="mr-sm-2">Zone Name:</label>
        <select class="form-control mb-2 mr-md-2" id="zone_name" required>
        </select>
        <label for="sensor_id" class="mr-sm-2">Sensor ID:</label>
        <select class="form-control mb-2 mr-md-2" id="sensor_id" required>
        </select>
        <button onclick="addZoneSensor()" class="btn btn-primary mb-2">Submit</button>
      </form>
    </div>
  </div>

  <?php 
  require_once 'assets/includes/footer.php';
  ?>
  <script src="assets/js/app.js"></script>
  <script>
    function onLoad(){
      getZoneSensors();
      getZonesList();
      getSensorsList();
    };
  </script>
</body>
</html>
