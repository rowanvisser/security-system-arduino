<?php 
require_once 'assets/includes/head.php';
?>

<body onload="onLoad()">

  <?php 
  require_once 'assets/includes/menu.php';
  ?>
  <div class="row content">
    <div class="col-sm-12 content-block">
      <h2>Central Zone</h2>
      <h6><b id="responseMessageDelete"></b></h6>
      <div class="table-responsive">          
        <table class="table table-dark table-striped" id="central-zone-table">
          <thead>
            <tr>
              <th>Central ID</th>
              <th>Zone Name</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <tr>

            </tr>
          </tbody>
        </table>
      </div>
      <hr>
      <h2>Add zone to Central</h2>
      <h6><b id="responseMessage"></b></h6>
      <form class="form-inline">
        <label for="central_id" class="mr-sm-2">Central ID:</label>
        <select class="form-control mb-2 mr-md-2" id="central_id" required>
        </select>
        <label for="zone_name" class="mr-sm-2">Zone Name:</label>
        <select class="form-control mb-2 mr-md-2" id="zone_name" required>
        </select>
        <button onclick="addCentralZone()" class="btn btn-primary mb-2">Submit</button>
      </form>
    </div>
  </div>

  <?php 
  require_once 'assets/includes/footer.php';
  ?>
  <script src="assets/js/app.js"></script>
  <script>
    function onLoad(){
      getCentralZone();
      getZonesList();
      getCentralList();
    };
  </script>
</body>
</html>
