<?php 
require_once 'assets/includes/head.php';
?>

<body onload="onLoad()">

  <?php 
  require_once 'assets/includes/menu.php';
  ?>

  <div class="row content">
    <div class="col-sm-12 content-block">
      <h2>Sensors</h2>
      <h6><b id="responseMessageDelete"></b></h6>
      <div class="table-responsive">          
        <table class="table table-dark table-striped" id="sensor_table">
          <thead>
            <tr>
              <th>Sensor ID</th>
              <th>Type</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <tr>

            </tr>
          </tbody>
        </table>
      </div>
      <hr>
      <h2>Add sensor</h2>
      <h6><b id="responseMessage"></b></h6>
      <form class="form-inline">
        <label for="sensor_id" class="mr-sm-2">Sensor ID:</label>
        <input type="text" class="form-control mb-2 mr-md-2" id="sensor_id" required>
        <label for="type" class="mr-sm-2">Type:</label>
        <input type="text" class="form-control mb-2 mr-md-2" id="type" required>
        <button onclick="addSensor()" class="btn btn-primary mb-2">Submit</button>
      </form>
    </div>
  </div>
  <?php 
  require_once 'assets/includes/footer.php';
  ?>
  <script src="assets/js/app.js"></script>
  <script>
    function onLoad(){
      getSensors();
    };
  </script>
</body>
</html>
