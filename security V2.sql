CREATE DATABASE security;
CREATE TABLE `logs`(
    `id` INT(11) NOT NULL,
    `zone_id` VARCHAR(45) NOT NULL,
    `sensor_id` VARCHAR(45) NOT NULL,
    `type` VARCHAR(45) NOT NULL,
    `description` VARCHAR(45) NOT NULL,
    `date` VARCHAR(250) NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8; --
 -- Gegevens worden geëxporteerd voor tabel `logs` --

INSERT
INTO
    `logs`(
        `id`,
        `zone_id`,
        `sensor_id`,
        `type`,
        `description`,
        `date`
    )
VALUES(
    16,
    '1',
    '85DE',
    'PirSensor',
    'StartupSensor',
    '20:04:21 | 20-11-2018'
),(
    17,
    '1',
    '85DE',
    'PirSensor',
    'Detected',
    '20:10:34 | 20-11-2018'
),(
    18,
    '1',
    '85DE',
    'PirSensor',
    'StartupSensor',
    '20:11:08 | 20-11-2018'
),(
    19,
    '1',
    '85DE',
    'PirSensor',
    'StartupSensor',
    '20:39:22 | 20-11-2018'
),(
    20,
    '1',
    '85DE',
    'PirSensor',
    'StartupSensor',
    '1:49:59:0 | 27-11-2018'
),(
    21,
    '1',
    '85DE',
    'PirSensor',
    'StartupSensor',
    '20:46:06 | 7-0-2019'
),(
    22,
    '1',
    '85DE',
    'PirSensor',
    'Detected',
    '20:55:18 | 7-0-2019'
),(
    23,
    '1',
    '85DE',
    'PirSensor',
    'StartupSensor',
    '23:52:07 | 7-1-2019'
),(
    24,
    '1',
    '85DE',
    'PirSensor',
    'StartupSensor',
    '9:48:21:0 | 8-1-2019'
),(
    25,
    '1',
    '85DE',
    'PirSensor',
    'Detected',
    '9:49:33:0 | 8-1-2019'
); -- -------------------------------------------------------- --
 -- Tabelstructuur voor tabel `sensors` --

CREATE TABLE `sensors`(
    `sensor_id` VARCHAR(4) NOT NULL,
    `type` VARCHAR(45) NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = latin1; --
 -- Gegevens worden geëxporteerd voor tabel `sensors` --

INSERT
INTO
    `sensors`(`sensor_id`, `type`)
VALUES('85DE', 'PirSensor'); -- -------------------------------------------------------- --
 -- Tabelstructuur voor tabel `users` --

CREATE TABLE `users`(
    `id` INT(11) NOT NULL,
    `username` VARCHAR(45) NOT NULL,
    `password` VARCHAR(45) NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = latin1; --
 -- Gegevens worden geëxporteerd voor tabel `users` --

INSERT
INTO
    `users`(`id`, `username`, `password`)
VALUES(1, 'admin', 'admin'); -- -------------------------------------------------------- --
 -- Tabelstructuur voor tabel `zones` --

CREATE TABLE `zones`(
    `zone_id` INT(11) NOT NULL,
    `status` INT(11) NOT NULL,
    `zone_name` VARCHAR(45) NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = latin1; --
 -- Gegevens worden geëxporteerd voor tabel `zones` --

INSERT
INTO
    `zones`(`zone_id`, `status`, `zone_name`)
VALUES(1, 1, 'Test Zone'); -- -------------------------------------------------------- --
 -- Tabelstructuur voor tabel `zone_sensor` --

CREATE TABLE `zone_sensor`(
    `id` INT(11) NOT NULL,
    `sensor_id` VARCHAR(4) NOT NULL,
    `zone_id` INT(11) NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = latin1; --
 -- Gegevens worden geëxporteerd voor tabel `zone_sensor` --

INSERT
INTO
    `zone_sensor`(`id`, `sensor_id`, `zone_id`)
VALUES(1, '85DE', 1); --
 -- Indexen voor geëxporteerde tabellen --
 --
 -- Indexen voor tabel `logs` --

ALTER TABLE
    `logs` ADD PRIMARY KEY(`id`); --
 -- Indexen voor tabel `users` --

ALTER TABLE
    `users` ADD PRIMARY KEY(`id`); --
 -- Indexen voor tabel `zones` --

ALTER TABLE
    `zones` ADD PRIMARY KEY(`zone_id`); --
 -- Indexen voor tabel `zone_sensor` --

ALTER TABLE
    `zone_sensor` ADD PRIMARY KEY(`id`); --
 -- AUTO_INCREMENT voor geëxporteerde tabellen --
 --
 -- AUTO_INCREMENT voor een tabel `logs` --

ALTER TABLE
    `logs` MODIFY `id` INT(11) NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 26; --
 -- AUTO_INCREMENT voor een tabel `users` --

ALTER TABLE
    `users` MODIFY `id` INT(11) NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 2; --
 -- AUTO_INCREMENT voor een tabel `zones` --

ALTER TABLE
    `zones` MODIFY `zone_id` INT(11) NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 3; --
 -- AUTO_INCREMENT voor een tabel `zone_sensor` --

ALTER TABLE
    `zone_sensor` MODIFY `id` INT(11) NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 3; 

CREATE TABLE `central` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `central_id` varchar(45) NOT NULL,
  PRIMARY KEY (`id`,`central_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

CREATE TABLE `central_zone` (
  `central_id` varchar(45) NOT NULL,
  `zone_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
