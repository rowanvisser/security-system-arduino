const express = require('express');
const app = express();
const port = process.env.PORT || 3000; //API port
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//Setting API Headers to allow cross link
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    next();
});
app.listen(port);

//Setting the routes
var routes = require('./api-v1/routes/router'); //importing route
routes(app); //register the route



// Default route
app.get('/', function (req, res) {
	console.log("Succesfull redirected to " + req.originalUrl);

    return res.send("Welcome to Security system API V1 " + req.originalUrl);

});

//4040 Error message
app.use(function(req, res) {
	console.log("ERROR " + req.originalUrl);

  res.status(404).send('Error, 404 Not Found on ' + req.originalUrl + '. This API does not have this specific call.')
});


console.log('REST API security system started on localhost port: ' + port);