var parser = require('xml2json');
var Request = require("request");

//Getting the weather in krommenie
exports.getWeather = function(req, res) {
  Request.get('http://weerlive.nl/api/json-data-10min.php?key=f8a741c6f2&locatie=Krommenie', (error, response, body) => {
    if(error){
      return console.log(error);
    }
    var obj = JSON.parse(body);
    var temperature = obj.liveweer[0].temp;
    res.json(temperature);
  });
};