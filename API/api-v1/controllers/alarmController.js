var parser = require('xml2json');
var Request = require("request");
const db = require('./dbController'); 
const sleep = (waitTimeInMs) => new Promise(resolve => setTimeout(resolve, waitTimeInMs));

//Check zone status where sensor is placed 
exports.sensorStatus = async function(req, res) {
  // console.log("Get sesnor power status");
  var sensor_id = req.params.sensor_id;
  var systemIsOn = false;
  await db.query("SELECT status FROM zones WHERE zone_id IN (SELECT zone_id FROM zone_sensor WHERE sensor_id = '" + sensor_id +"')", (err, result, fields) => {
    if(err) throw err;
    for(var i = 0; i < result.length; i++){
      if(result[i].status == 1){
        systemIsOn = true;
      }
    }
    if(systemIsOn){
      res.status(200).send((1).toString());
    }else{
      res.status(200).send((0).toString());   
    }
  });
};
//Add sensor
exports.addSensor = function(req, res) { //Done
  console.log("Add sensor");
  var sensor_id = req.params.sensor_id;
  var type = req.params.type;
    db.query("INSERT INTO sensors (sensor_id, type) VALUES ('" + sensor_id + "', '" + type + "')" , (err, result) => {
      if(err){
        res.sendStatus(404);
      }else{
        console.log('Added sensor id: ' + result);
        res.sendStatus(200);
      }
    });
};
//Get all zones
exports.getSensors = function(req, res) { //DONE
  console.log("Get sensor");
  db.query("SELECT * FROM sensors", (err, result, fields) => {
    if(err) throw err;
    console.log("Return sensors", result);
    res.json(result);
  });
};
//Delete sensor 
exports.deleteSensor = function(req, res) { //Done
  console.log("Delete sensor");
  var sensor_id = req.params.sensor_id;
    db.query("DELETE FROM sensors WHERE sensor_id = '"+ sensor_id + "'", (err, result) => {
      if(err) throw err;
      console.log('Deleted sensor', sensor_id);
    });
  res.sendStatus(200);
};


//Add zone
exports.addZone = function(req, res) { //Done
  console.log("Add zone");
  var zone_name = req.params.zone_name;
    db.query("INSERT INTO zones (status, zone_name) VALUES ('0', '" + zone_name + "')" , (err, result) => {
      if(err) throw err;
      console.log('Added zone id: ' + result.insertId);
    });
  res.sendStatus(200);
};
//Get all zones
exports.getZones = function(req, res) { //DONE
  console.log("Get zone");
  db.query("SELECT * FROM zones", (err, result, fields) => {
    if(err) throw err;
    console.log("Return zones", result);
    res.json(result);
  });
};
//Update zone status
exports.switchZoneStatus = function(req, res) { //Done
  console.log("Switch system power");
  var zone_id = req.params.zone_id;
  var status = req.params.status;
  db.query(
    'UPDATE zones SET status = ? Where zone_id = ?',
    [status, zone_id],
    (err, result) => {
      if (err) throw err;
      console.log('Changed power status to ' + status);
    }
  );
  res.sendStatus(200);
};
//Delete zone 
exports.deleteZone = function(req, res) { //Done
  console.log("Delete zone");
  var zone_id = req.params.zone_id;
    db.query("DELETE FROM zones WHERE zone_id = "+ zone_id, (err, result) => {
      if(err) throw err;
      console.log('Deleted zone', zone_id);
    });
    db.query("DELETE FROM zone_sensor WHERE zone_id = "+ zone_id, (err, result) => {
      if(err) throw err;
      console.log('Deleted zone', zone_id);
    });
  res.sendStatus(200);
};


//Add zonesensor
exports.addZoneSensor = function(req, res) { //Done
  console.log("Add zone");
  var sensor_id = req.params.sensor_id;
  var zone_id = req.params.zone_id;
    db.query("INSERT INTO zone_sensor (zone_id, sensor_id) VALUES ('" + zone_id + "', '" + sensor_id + "')" , (err, result) => {
      if(err) throw err;
      console.log('Added sensor id: ' + result.insertId);
    });
  res.sendStatus(200);
};
//Get all zones sensors
exports.getZoneSensor = function(req, res) { //DONE
  console.log("Get zone_sensor");
  db.query("SELECT *, zone_name FROM zone_sensor zs INNER JOIN zones z ON zs.zone_id = z.zone_id ORDER BY z.zone_name", (err, result, fields) => {
    if(err) throw err;
    console.log("Return zone_sensors", result);
    res.json(result);
  });
};
//Delete zone sensor
exports.deleteZoneSensor = function(req, res) { //Done
  console.log("Delete zone sensor");
  var zoneSensor_id = req.params.zoneSensor_id;
    db.query("DELETE FROM zone_sensor WHERE id = "+ zoneSensor_id, (err, result) => {
      if(err) throw err;
      console.log('Deleted zone sensor', zoneSensor_id);
    });
  res.sendStatus(200);
};


//Add central system
exports.addCentralSystem = function(req, res) { //Done
  console.log("Add Central System");
  var central_id = req.params.central_id;
    db.query("INSERT INTO central (central_id) VALUES ('" + central_id + "')" , (err, result) => {
      if(err) throw err;
      console.log('Added central System id: ' + result.insertId);
    });
  res.sendStatus(200);
};

//Get all Centrals
exports.getCentrals = function(req, res) { //DONE
  console.log("Get Central");
  db.query("SELECT * FROM central", (err, result, fields) => {
    if(err) throw err;
    console.log("Return central", result);
    res.json(result);
  });
};

//Delete Central 
exports.deleteCentral = function(req, res) { //Done
  console.log("Delete central");
  var central_id = req.params.central_id
    db.query("DELETE FROM central WHERE id = "+ central_id, (err, result) => {
      if(err) throw err;
      console.log('Deleted central', central_id);
    });
    db.query("DELETE FROM central_zone WHERE central_id IN (SELECT central_id FROM central WHERE id = "+ central_id + ")",  (err, result) => {
      if(err) throw err;
      console.log('Deleted Central Zone', central_id);
    });
  res.sendStatus(200);
};



//Add zone to central
exports.addCentralZone = function(req, res) { //Done
  console.log("Add Central Zone");
  var central_id = req.params.central_id;
  var zone_id = req.params.zone_id;
    db.query("INSERT INTO central_zone (zone_id, central_id) VALUES ('" + zone_id + "', '" + central_id + "')" , (err, result) => {
      if(err) throw err;
      console.log('Added central zone id: ' + result.insertId);
    });
  res.sendStatus(200);
};

// Get all central zones 
exports.getCentralZone = function(req, res) { //DONE
  console.log("Get central_zones");
  db.query("SELECT *, zone_name FROM central_zone cz INNER JOIN zones z ON cz.zone_id = z.zone_id ORDER BY z.zone_name", (err, result, fields) => {
    if(err) throw err;
    console.log("Return central_zones", result);
    res.json(result);
  });
};

//Delete zone sensor
exports.deleteCentralZone = function(req, res) { //Done
  console.log("Delete central zone");
  var zone_id = req.params.zone_id;
  var central_id = req.params.central_id;
    db.query("DELETE FROM central_zone WHERE zone_id = '" + zone_id + "' AND central_id = '" + central_id + "'", (err, result) => {
      if(err) throw err;
      console.log('Deleted central zone', central_id, " ", zone_id);
    });
  res.sendStatus(200);
};

//Central check for new detections
exports.checkForNewDetection = async function(req, res) { //Done
  var central_id = req.params.central_id;
  console.log("Check for new Detection for a central " + central_id);
  var detected = false;
  var zones = [];
  //First gets all zones that are assigned to this central system
  db.query("SELECT zone_name FROM zones z INNER JOIN central_zone c ON z.zone_id = c.zone_id WHERE z.status = 1 AND c.central_id = '" + central_id +"'", (err, result, fields) => {
    if(err) throw err;
    console.log("Return zone_sensors", result.length);
    if(result.length > 0) {
      zones = result;
    } else {
      res.status(200).send((0).toString());
    }
    console.log("ACTIVE ZONES " + zones);

  });
  await sleep(2000);
  for (var i = 0; i < zones.length; i++) {
      await db.query("SELECT timestamp, description FROM logs l INNER JOIN zones z ON l.zone_id = z.zone_id WHERE z.zone_name = '" + zones[i].zone_name + "'", (err, result, fields) => {
        if(err) throw err;
        var timestamp = Date.now() - 60000;
        for (var i = 0; i < result.length; i++) {
          if(result[i].timestamp > timestamp && result[i].description == "Detected") {
            console.log("Check for new Detection for a central DETECTED " + central_id);
            res.status(200).send((1).toString());
            i = result.length + 1;
            detected = true;
          }else if(i == result.length - 1) {
              console.log("Check for new Detection for a central NOT DETECTED " + central_id);
              res.send((0).toString());
          }
        };
      });

  };
};

//Check if the zones are turned off when there is a detection so the central knows when to stop
exports.checkIfZonesAreOff = function(req, res) { //Done
  console.log("Check while detected something if the zones are turned off so the central could stop making noise");
  var central_id = req.params.central_id;
  //First gets all zones that are assigned to this central system
  db.query("SELECT * FROM zones z INNER JOIN central_zone c ON z.zone_id = c.zone_id WHERE z.status = 1 AND c.central_id = '" + central_id +"'", (err, result, fields) => {
    if(err) throw err;
    console.log("Return zone_sensors", result);
    if(result.length > 0){
      res.status(200).send((1).toString());
    }else{
      res.status(200).send((0).toString());
    }
  });
};


