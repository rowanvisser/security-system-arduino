var parser = require('xml2json');
var Request = require("request");
const db = require('./dbController'); 

//Getting the logs, giving 0 returns everything otherwise it is a limit
exports.getLogs = function(req, res) { //Done
  console.log("Get logs");  
  var amount = req.params.amount;
  var type = req.params.type;
  var query = "";
  if(amount == 0){
    query = "SELECT *, zone_name FROM logs l INNER JOIN zones z ON l.zone_id = z.zone_id ORDER BY date ASC";
  }else if(type == "detection"){
    query = "SELECT *, zone_name FROM logs l INNER JOIN zones z ON l.zone_id = z.zone_id ORDER BY date ASC limit " + amount;
  }
  db.query(query, (err, result, fields) => {
    if(err) throw err;
    console.log(result);
    var output = result;
    
    console.error("Get logs " + output);
    res.json(output);
  });
};

//Adding logs to database
exports.addLogs = async function(req, res) { //DONE
  console.log("Add logg to database");
  var sensor_id = req.params.sensor_id;
  var description = req.params.description;
  var timeNow = new Date();
  var hours   = timeNow.getHours();
  var minutes = timeNow.getMinutes();
  var seconds = timeNow.getSeconds();
  var day = timeNow.getDate();
  var month = timeNow.getMonth();
  var year = timeNow.getFullYear();
  var timeString = "" + hours;
  timeString  += ((minutes < 10) ? ":0" : ":") + minutes;
  timeString  += ((seconds < 10) ? ":0" : ":") + seconds;
  timeString  += ((hours < 10) ? ":0" : "");
  timeString += " | " + day + "-" + (month + 1) + "-" + year;
  await db.query("SELECT type, zone_id FROM sensors s INNER JOIN zone_sensor z ON s.sensor_id = z.sensor_id WHERE s.sensor_id LIKE '" + sensor_id + "'", (err, result) => {
    if(err) throw err;
      if(result[0].type != null){
        var type = result[0].type;
        var zone_id = result[0].zone_id;
        db.query("INSERT INTO logs (zone_id, sensor_id, type, description, date, timestamp) VALUES ('" + zone_id + "', '" + sensor_id + "', '" + type + "', '" + description + "', '" + timeString + "', '" + Date.now() + "')" , (err, result) => {
          if(err) throw err;
          console.log('Added log id: ' + result.insertId);
          res.sendStatus(200);
        });
      }
  });
};