'use strict';
module.exports = function(app) {
    var logController = require('../controllers/logController');
    var alarmController = require('../controllers/alarmController');
    var userController = require('../controllers/userController');
    var weatherController = require('../controllers/weatherController');


    //Sensor routes
    app.route('/v1/power/sensor/:sensor_id')
    .get(alarmController.sensorStatus);
    app.route('/v1/sensor/:sensor_id/:type')
    .post(alarmController.addSensor);
    app.route('/v1/sensors/')
    .get(alarmController.getSensors);
    app.route('/v1/sensor/:sensor_id')
    .post(alarmController.deleteSensor);

    //Central system calls
    app.route('/v1/central/')
    .get(alarmController.getCentrals);
    app.route('/v1/central/:central_id')
    .post(alarmController.addCentralSystem);
    app.route('/v1/central/:central_id/delete')
    .post(alarmController.deleteCentral);

    //Central Zone
    app.route('/v1/central-zone/')
    .get(alarmController.getCentralZone);
    app.route('/v1/central-zone/:central_id/:zone_id')
    .post(alarmController.deleteCentralZone);
    app.route('/v1/central/:central_id/:zone_id')
    .post(alarmController.addCentralZone);

    //Detection for zone and central
    app.route('/v1/central/:central_id')
    .get(alarmController.checkForNewDetection);
    app.route('/v1/central/power/:central_id')
    .get(alarmController.checkIfZonesAreOff);

    //Zone routes
    app.route('/v1/zone/:zone_name/add')
    .post(alarmController.addZone);
    app.route('/v1/zone/')
    .get(alarmController.getZones);
    app.route('/v1/power/zone/:zone_id/:status')
    .post(alarmController.switchZoneStatus);
    app.route('/v1/zone/:zone_id/delete')
    .post(alarmController.deleteZone);

    //Zone-sensor table routes
    app.route('/v1/zone-sensor/')
    .get(alarmController.getZoneSensor);
    app.route('/v1/zone-sensor/:zone_id/:sensor_id')
    .post(alarmController.addZoneSensor);
    app.route('/v1/zone-sensor/:zoneSensor_id')
    .post(alarmController.deleteZoneSensor);

    //Get and add logs
    app.route('/v1/logs/:type/:amount')
    .get(logController.getLogs);
    app.route('/v1/logs/:sensor_id/:description')
    .post(logController.addLogs);

    //Login Routes
    app.route('/v1/user/:username/:password')
    .post(userController.getUser);

    //External weather route
    app.route('/v1/weather')
    .get(weatherController.getWeather);
};


